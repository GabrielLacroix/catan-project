package utils;

import java.util.Random;

public class Dice
{
	public static int roll()
	{
		Random random = new Random();
		return random.ints(2, 13).findFirst().orElse(0);
	}
}
