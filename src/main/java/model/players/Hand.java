package model.players;

import model.game.Resource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Hand
{
	private ArrayList<Resource> _cards;
	
	Hand()
	{
		_cards = new ArrayList<>();
	}
	
	public void addCards(Resource ... resources)
	{
		_cards.addAll(Arrays.asList(resources));
	}
	
	public void inspect()
	{
	
	}
	
	public List<Resource> getCards()
	{
		return _cards;
	}
}
