package model.game;

public enum Resource
{
	Brick,
	None,
	Sheep,
	Stone,
	Wheat,
	Wood;
}
