package model.game;

import model.players.Player;
import utils.Dice;

import java.util.ArrayList;
import java.util.List;

public class Game
{
	private static final int SCORE_MAX = 10;
	Board _board;
	List<Player> _players;
	
	
	public Game()
	{
		_board = new Board();
		_players = new ArrayList<>();
	}
	
	public void play()
	{
		while (!isGameFinished())
		{
			int roll = Dice.roll();
			
		}
	}
	
	private boolean isGameFinished()
	{
		for (Player p : _players)
		{
			if (p.getScore() >= SCORE_MAX)
				return true;
		}
		return false;
	}
}
