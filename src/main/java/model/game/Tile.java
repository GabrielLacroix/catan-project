package model.game;

interface Tile
{
	Resource getResource();
	
	int getValue();
}
