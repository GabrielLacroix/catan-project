package model.game;

public interface Port
{
	Resource getType();
}
